local on_attach = require("user.lsp.handlers").on_attach
local capabilities = require("user.lsp.handlers").capabilities

require("mason").setup({
	ui = {
		icons = {
			package_installed = "✓",
		},
	},
})
require("mason-lspconfig").setup({
	ensure_installed = { "lua_ls", "pylsp", "pyright" },
	automatic_installation = true,
})

require("lspconfig").lua_ls.setup({ on_attach = on_attach, capabilities = capabilities })
require("lspconfig").pyright.setup({
	on_attach = on_attach,
	capabilities = capabilities,
	settings = {
		python = {
			analysis = { diagnosticMode = "off", typeCheckingMode = "off" },
		},
	},
})
